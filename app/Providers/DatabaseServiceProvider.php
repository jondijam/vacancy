<?php

namespace App\Providers;

use App\Vacancy\Api;
use App\Vacancy\Api\RestApi;
use Illuminate\Support\ServiceProvider;


class DatabaseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Api::class, RestApi::class);
    }
}
