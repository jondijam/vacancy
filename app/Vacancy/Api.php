<?php
namespace App\Vacancy;

interface Api
{
    public function getData($api, $user = null, $passw = null);
}