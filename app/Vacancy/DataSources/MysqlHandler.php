<?php
namespace App\Vacancy\DataSources;

use App\Vacancy\Observer;
use App\Vacancy;
use Illuminate\Config\Repository;

/**
 * Created by PhpStorm.
 * User: jondijam
 * Date: 15.1.2016
 * Time: 09:02
 */
/**
 * Mysql database is one type of data source the user can possible use in the project.
 * I created an object that handle the mysql driver.
 * At the moment you add single database.
 */
class MysqlHandler implements Observer
{
    protected $connection = "";
    protected $database = "";

    /**
     * add single database
     * @param string $database
    */
    public function __construct($database = "default")
    {
        $this->database = $database;
    }

    public function handle($data)
    {
        $vacancy = new Vacancy();
        $this->connection = $vacancy->setConnection($this->database);
    }

    public function create($title,$content,$description)
    {
        $this->connection->create(['title'=>$title, 'conttent'=>$content, 'description'=>$description]);
    }

    public function delete($id)
    {
        $this->connection->findOrNew($id)->delete($id);
    }


    /**/
    public function find($id)
    {
        return $this->connection->findOrNew($id)->delete($id);
    }

    private function FilterIfDatabaseExist($connections)
    {
        $config = config('database.connections');
        $result = !empty(array_intersect($config, $connections));
        return $result;
    }
}



