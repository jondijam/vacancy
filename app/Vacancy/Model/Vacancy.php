<?php
namespace App\Vacancy\Model;

use App\Vacancy\VacancyRepository;
use App\Vacancy\Api;

class Vacancy
{
    private $dataSources;

    /**
     * The id of the vacancy
     *
     * @var integer
     */
    public $id;

    /**
     * The vacancy title
     *
     * @var string
     */
    public $title;

    /**
     * The vacancy content/description
     *
     * @var string
     */
    public $content;

    /**
     * The vacancy description
     *
     * @var string
     */
    public $description;
    protected $repository;

    public function loadVacation($id)
    {
        $restApi = new Api\RestApi();
        $repository = new VacancyRepository($restApi);
        return $repository->loadData($id);
    }
}