<?php
/**
 * Created by PhpStorm.
 * User: jondijam
 * Date: 14.1.2016
 * Time: 17:06
 */

/*
 * In the test project I have to create a object that import Api and save the data to multiple datasources
 * Datasources can be in any type of format
 * On this object I follow observer design patter. Now many people ask why
 *
 * I create an object for each type of datasources. For example xml file or a txt file can be a datasource as well
 *
 *
 *
 * You could have many types of Api. For example soap or restful api
 * Add datasources to array. It could be one or more datasources
 * Each datasources need handle multiple databases.
*/

namespace App\Vacancy;
use App\Vacancy\Api;

class VacancyRepository implements Subject
{

    protected $api;
    protected $observers = [];

    /**
     * Assign api to property.
    */
    public function __construct(Api $api)
    {
        $this->api = $api;
    }

    /**
     * Attach a new data source to a an array.
     * @param array|string $observable
     */
    public function attach($observable)
    {
        // TODO: Implement attach() method.
        if(is_array($observable))
        {
            $this->attacheServers($observable);
        }

        $this->observers[] = $observable;
        return;
    }

    /**
     * Here you can detatch a datasource from the list
    */
    public function detach($index)
    {
        // TODO: Implement detach() method.
        unset($this->observers[$index]);
    }

    /**
     * if observable is an array created a loop to loop through the array.
     */
    protected function attacheServers($observable)
    {
        foreach($observable as $observer)
        {
            if(!$observer instanceof Observer)
                throw new \Exception();
            $this->attach($observer);
        }

        $this->observers[] = $observable;
        return;
    }

    /**
     * Import the data from api.
    */
    public function fire($api, $user = null, $password = null)
    {
        $data = $this->api->getData($api, $user, $password);
        return $this->notify($data);
    }

    /**
     * Load data from data sources. If the data source is not in the list return null.
    */
    public function loadData($id)
    {
        if (!empty($this->observers))
        {
            foreach($this->observers as $observer)
            {
                return $observer->loadData($id);
            }
        }

        return null;
    }

    /**
     * Handle the data source.
     * @return mixed
     */
    public function notify($data)
    {
        if (!empty($this->observers))
        {
            foreach($this->observers as $observer)
            {
                return $observer->handle($data);
            }
        }

        return null;
    }
}
