<?php
namespace App\Vacancy;

interface Observer
{
    public function handle($data);
    public function create($title,$content,$description);
    public function delete($id);
    public function find($id);
}