<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Vacancy\DataSources\CacheHandle;
use App\Vacancy\VacancyRepository;
use App\Vacancy\Model\Vacancy;
use App\Vacancy\DataSources\MysqlHandler;

class VacancyTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testVacancyModel()
    {
        $database = "vacancy";
        $mysql = new MysqlHandler($database);
        $cache = new CacheHandle();
        $api = "";
        $restApi = new \App\Vacancy\Api\RestApi();
        $repositories = new VacancyRepository($restApi);
        $dataSources = [$mysql, $cache];
        $repositories->attach($dataSources);

        $vacancy = new Vacancy();
        $id = 1024;
        $vacation = $vacancy->loadVacation($id);

        $this->assertTrue($vacation === 1024);
    }

    public function testImportApiToDataSource()
    {
        $database = "vacancy";
        $mysql = new MysqlHandler($database);
        $cache = new CacheHandle();
        $api = "";
        $restApi = new \App\Vacancy\Api\RestApi();
        $repositories = new VacancyRepository($restApi);
        $dataSources = [$mysql, $cache];
        $repositories->attach($dataSources);
        $fire = $repositories->fire($api);

        //mysql handle works
        $this->assertTrue($fire == 2024);
    }
}

/*
 *   $mysql = new MysqlHandler($database);
        $cache = new CacheHandle();
        $repositories->attach([$mysql, $cache]);
        $repositories->fire($api);

        $vacancy = new Vacancy();

        $vacation_ids = array(1234,3,4,2,4321,45,32,5533);

        foreach ($vacation_ids as $id)
        {
            $vacancy->loadVacation($id);
        }
 * */